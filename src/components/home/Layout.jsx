import React from 'react'
import "./base.css"

const Layout = ({children}) => {
  return (
    <section className="bookstore-section container-fluid">
      <aside className="bookstore-menu">
        <div>
          <div className="bs-menu--header">
            <img src={require("../../resources/images/profil.png").default} alt="profil" />
            <span>Dilane</span>
          </div>

          <div className="bs-menu--nav">
            <ul>
              <li className="active">
                <i className="bi bi-search"></i>
                <span>Explorer</span>
              </li>
              <li>
                <i className="bi bi-search"></i>
                <span>Categorie</span>
              </li>
              <li>
                <i className="bi bi-book"></i>
                <span>Mes Livres</span>
              </li>
            </ul>
          </div>
        </div>

        <div>
          <span>Recemment lu</span>

          <div className="bs-currently-read">
            <img src={require("../../resources/images/profil.png").default} alt="profil" />

            <div className="bs-currently-read--info">
              <div className="info-top">
                <span>La Jungle</span>
                <span>Paul Ardi</span>
              </div>
              <div className="info-bottom">
                124 Pages
              </div>
            </div>
          </div>
        </div>
      </aside>

      <section className="bookstore-menu--mobile">
        <nav>
          <a className="active" href="#">
            <i className="bi bi-search"></i>
            <span>Explorer</span>
          </a>
          <a href="#">
            <i className="bi bi-search"></i>
            <span>Categorie</span>
          </a>
          <a href="#">
            <i className="bi bi-book"></i>
            <span>Mes Livres</span>
          </a>
        </nav>
        <img src={require("../../resources/images/profil.png").default} alt="profil" />
      </section>

      <section className="bookstore-main"> {children} </section>
    </section>
  )
}

export default Layout
