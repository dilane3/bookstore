import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Login from './security/Login'
import Explore from './home/Explore'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact render={() => <Explore />} />
        <Route path="/login" render={() => <Login />} />
      </Switch>
    </Router>
  )
}

export default App;
