import React, {useEffect} from 'react'
import "./login.css"

const Login = () => {
  useEffect(() => {
    const formInputs = document.querySelectorAll(".form-input")
    const btns = document.querySelectorAll(".btn")
    const btnSignin = document.querySelector(".btn-signin")
    const btnSignup = document.querySelector(".btn-signup")
    const signinForm = document.querySelector(".signin")
    const signupForm = document.querySelector(".signup")

    formInputs.forEach(formInput => {
      formInput.childNodes[0].onfocus = function() {
        formInput.childNodes[1].classList.add("input-focused")
        formInput.childNodes[0].style.color = "blue"
        formInput.childNodes[0].style.fontWeight = "bold"
      }

      formInput.childNodes[0].onblur = function() {
        formInput.childNodes[1].classList.remove("input-focused")
        formInput.childNodes[0].style.color = "#000"
        formInput.childNodes[0].style.fontWeight = "500"
      }

      if (formInput.childNodes[2] !== undefined) {
        formInput.childNodes[2].onclick = function() {
          if (formInput.childNodes[0].type === "text") {
            formInput.childNodes[0].type = "password"
            this.classList.add("bi-eye-slash")
            this.classList.remove("bi-eye")
          } else {
            formInput.childNodes[0].type = "text"
            this.classList.add("bi-eye")
            this.classList.remove("bi-eye-slash")
          }
        }
      }
    })

    btns.forEach(btn => {
      btn.onclick = function(e) {
        e.preventDefault()

        btn.classList.add("btn-focused")

        let time = setTimeout(() => {
          btn.classList.add("hidden")
          btn.classList.remove("btn-focused")

          let time2 = setTimeout(() => {
            btn.classList.remove("hidden")

            clearTimeout(time2)
          }, 800)

          clearTimeout(time)
        }, 1000)
      }
    })

    btnSignup.onclick = function() {
      signupForm.classList.add("mask-form")

      let time = setTimeout(() => {
        signupForm.classList.add("d-none")
        signinForm.classList.remove("d-none")
        signinForm.classList.remove("mask-form")
      }, 100)
    }

    btnSignin.onclick = function() {
      signinForm.classList.add("mask-form")

      let time = setTimeout(() => {
        signinForm.classList.add("d-none")
        signupForm.classList.remove("d-none")
        signupForm.classList.remove("mask-form")
      }, 100)
    }
  })

  return (
    <section className="container-fluid">
      <article className="form signin d-none">
        <header className="form-header">
          <h3>BookStore</h3>
          <span>Signin</span>
        </header>

        <div className="form-content">
          <div className="form-input">
            <input type="text" placeholder="Login" />
            <span></span>
          </div>

          <div className="form-input">
            <input type="password" placeholder="Password" />
            <span></span>
            <i className="bi bi-eye-slash"></i>
          </div>

        </div>

        <div className="form-footer">
          <button className="btn">SIGNIN</button>
          <span>Pas encore de compte editeur ? <a className="btn-signin">Creer un compte</a></span>
        </div>
      </article>

      <article className="form signup">
        <header className="form-header">
          <h3>BookStore</h3>
          <span>Signup</span>
        </header>

        <div className="form-content">
          <div className="form-input">
            <input type="text" placeholder="Login" />
            <span></span>
          </div>

          <div className="form-input">
            <input type="email" placeholder="Email" />
            <span></span>
          </div>

          <div className="form-input">
            <input type="password" placeholder="Password" />
            <span></span>
            <i className="bi bi-eye-slash"></i>
          </div>

        </div>

        <div className="form-footer">
          <button className="btn">SIGNUP</button>
          <span>Deja un compte ? <a className="btn-signup">Connectez-vous</a></span>
        </div>
      </article>
    </section>
  )
}

export default Login
